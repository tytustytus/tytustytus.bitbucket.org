function normalizeName(name) {
  name = name.trim();
  name = name.replace(/ +(?= )/g, '').replace(/ /g, '_').replace(/[^a-zA-Z0-9_]/g, '');
  name = name.toLocaleLowerCase();
  return name;
}

function getCampaigName(campaignName, date, campaignDateRequiredRadio) {
  if (campaignDateRequiredRadio === 'false') {
    return normalizeName(campaignName);
  } else {
    return normalizeName(campaignName) + '_' + date.replace(new RegExp('/', 'g'), '_');

  }
}

function generateLink(utmData) {

  const {
    websiteUrl,
    campaignSource,
    campaignMedium,
    campaignName,
    campaignContent,
    campaignDateRequiredRadio,
    date
  } = utmData;

  let url = websiteUrl,
    urlPrefix = '?',
    searchQuery = 'utm_source=' + normalizeName(campaignSource) + '&utm_medium=' + normalizeName(campaignMedium) + '&utm_campaign=' + getCampaigName(campaignName, date, campaignDateRequiredRadio);

  if (campaignContent) {
    searchQuery += '&utm_content=' + normalizeName(campaignContent);
  }

  if (url.indexOf('?') > -1) {
    urlPrefix = '&';
  }

  return url + urlPrefix + searchQuery;

}

function getCurrentDate() {
  const d = new Date();
  return (d.getDate() < 10 ? '0' + (d.getDate() + 1) : d.getDate()) + '/' + (d.getMonth() < 10 ? '0' + (d.getMonth() + 1) : d.getMonth()) + '/' + d.getFullYear().toString().substr(-2);
}

$("#date").val(getCurrentDate());
/*
$("#utmBuilder").on('submit', function(){
  const websiteUrl = $("#websiteUrl").val(),
    campaignSource = $("#campaignSource").val(),
    campaignMedium = $("#campaignMedium").val(),
    campaignName = $("#campaignName").val(),
    campaignContent = $("#campaignContent").val(),
    date = $("#date").val();
  generateLink({
    websiteUrl,
    campaignSource,
    campaignMedium,
    campaignName,
    campaignContent,
    date
  });
});
*/

$("#utmBuilder").parsley({
  errorClass: 'is-invalid text-danger',
  successClass: 'is-valid', // Comment this option if you don't want the field to become green when valid. Recommended in Google material design to prevent too many hints for user experience. Only report when a field is wrong.
  errorsWrapper: '<span class="form-text text-danger"></span>',
  errorTemplate: '<span></span>',
  trigger: 'change'
}).on('form:success', () => {
  const websiteUrl = $("#websiteUrl").val(),
    campaignSource = $("#campaignSource").val(),
    campaignMedium = $("#campaignMedium").val(),
    campaignName = $("#campaignName").val(),
    campaignContent = $("#campaignContent").val(),
    campaignDateRequiredRadio = $('input[name=campaignDateRequiredRadio]:checked').val(),
    date = $("#date").val();
  const outputLink = generateLink({
    websiteUrl,
    campaignSource,
    campaignMedium,
    campaignName,
    campaignContent,
    campaignDateRequiredRadio,
    date
  });

  const outputCampaignName = getCampaigName(campaignName, date, campaignDateRequiredRadio);

  $("#outputLink").val(outputLink);
  $("#outputCampaignName").val(outputCampaignName);

  return false;

});


const templates = {
  none: {
    campaignSource: '',
    campaignMedium: '',
    campaignName: '',
    campaignContent: '',
  },
  jo_pracuj: {
    campaignSource: 'pracuj.pl',
    campaignMedium: 'job offer',
    campaignName: 'job offer',
    campaignContent: '',
  },
  jo_fb_praca_it: {
    campaignSource: 'facebook.com',
    campaignMedium: 'job offer',
    campaignName: 'job offer',
    campaignContent: 'praca it w szczecinie',
  },
  jo_fb_praca_it_free: {
    campaignSource: 'facebook.com',
    campaignMedium: 'job offer',
    campaignName: 'job offer',
    campaignContent: 'praca it w szczecinie free',
  },
  jo_fb_ad: {
    campaignSource: 'facebook.com',
    campaignMedium: 'ad',
    campaignName: 'job offer',
    campaignContent: '',
  },
  fb_organic: {
    campaignSource: 'facebook',
    campaignMedium: 'social',
    campaignName: 'blogpost | video | image',
    campaignContent: 'Skrócona nazwa posta/video/obrazka ze spacjami',
    campainContentRequired: true,
    campainDateRequired: false
  },
  fb_ads: {
    campaignSource: 'facebook',
    campaignMedium: 'cpc',
    campaignName: 'blogpost | video | image nazwa kampanii ze spacjami',
    campaignContent: 'Skrócona nazwa posta',
    campainContentRequired: true,
  },
  twitter_organic: {
    campaignSource: 'twitter',
    campaignMedium: 'social',
    campaignName: 'blogpost | video | image',
    campaignContent: 'Skrócona nazwa posta/video/obrazka ze spacjami',
    campainContentRequired: true,
    campainDateRequired: false
  },
  twitter_ads: {
    campaignSource: 'twitter',
    campaignMedium: 'cpc',
    campaignName: 'blogpost | video | image nazwa kampanii',
    campaignContent: 'Skrócona nazwa posta/video/obrazka ze spacjami',
    campainContentRequired: true,
  },
  google_ads: {
    campaignSource: 'google',
    campaignMedium: 'cpc',
    campaignName: 'blogpost | video | image nazwa kampanii',
    campaignContent: 'Skrócona nazwa posta/video/obrazka ze spacjami',
    campainContentRequired: true,
  },
  google_display: {
    campaignSource: 'google',
    campaignMedium: 'display',
    campaignName: 'blogpost | video | image nazwa kampanii',
    campaignContent: 'Skrócona nazwa posta/video/obrazka ze spacjami',
    campainContentRequired: true,
  },
}

$("#template").on('change', function () {

  const $this = $(this);
  const templateValue = $this.val();

  const selectedTemplate = templates[templateValue];

  if (!selectedTemplate) {
    return;
  }

  if (selectedTemplate.campainContentRequired && selectedTemplate.campainContentRequired == true) {
    $('#campaignContentLabel').html('<strong>Campaign Content</strong>');
  } else {
    $('#campaignContentLabel').html('Campaign Content (optional)');
  }
  $("#template option[value=" + templateValue + "]").attr('selected', 'selected');
  $("#campaignSource").val(selectedTemplate.campaignSource);
  $("#campaignMedium").val(selectedTemplate.campaignMedium);
  $("#campaignName").val(selectedTemplate.campaignName);
  $("#campaignContent").val(selectedTemplate.campaignContent);

  if (selectedTemplate.campainDateRequired == false)
    $("#campaignDateNo").prop("checked", true);
  else {
    $("#campaignDateYes").prop("checked", true);
  }

  $('#template > option:first').attr('selected', true);

});