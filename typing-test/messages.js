function addNewMessage(type, message){
	var alertType = 'secondary';
	if(type=='bot'){
		alertType = 'primary';
	}
	$("#message-container").append('<div class="alert alert-' + alertType + '" role="alert" style="display: none;"> ' + message + ' </div>');
	$("#message-container > .alert:last").slideDown(300);
}

function changeMode(mode){
	location.href = 'index.html?mode=' + mode;
}

function getMode(){
	// delay = 10s
	// none = no
	// typing = when is typing + 10s of delay
	var mode = 'typing';
	var search = location.search.substr(1).split('=');
	if(search[0]=='mode'){
		mode = search[1];
	}
	return mode;
}


var messageTriggerCaller = null;

function messageTriggerCall(){
	clearTimeout(messageTriggerCaller);
	var delay = 5 * 1000;
	if(getMode()=='none'){
		delay = 1;
	}
	messageTriggerCaller = setTimeout(function(){
		addNewMessage('bot', 'Bot response');
		$('body').removeClass('bot-is-typing');
	}, delay);
}

var messageQueue = [];

$("#message-form").on('submit', function(ev){
	ev.preventDefault();
	var message = $("#message-form-message").val();
	if(!message){
		return ;
	}
	addNewMessage('user', message);
	messageQueue.push(message);
	$("#message-form-message").val('');
	messageTriggerCall();
});

$("#message-form-message").on('keyup', function(){
	if(getMode()!=='typing'){
		return false;
	}
	messageTriggerCall();
});
