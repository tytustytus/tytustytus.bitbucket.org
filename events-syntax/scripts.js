function toTitleCase(str) {
  return str.replace(/\w\S*/g, function(txt){
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
}


function normalizeEventName(name){
  name = name.trim();
  name = name.replace(/\s{2,}/g, ' ');
  name = toTitleCase(name);
  return name;
}

function normalizeEventProperty(name){
  name = name.trim();
  name = name.replace(/ +(?= )/g,'').replace(/ /g, '_').replace(/[^a-zA-Z0-9_]/g, '');
  name = name.toLocaleLowerCase();
  return name;
}

$("#eventBuilder").on('submit', function(){
  let eventName = $("#eventName").val(),
    eventProperties = $("#eventProperties").val();


  const $eventNameOutput = $("#eventNameOutput"),
        $eventPropertiesOutput = $("#eventPropertiesOutput"),
        $eventPropertiesOutputRaw = $("#eventPropertiesOutputRaw");


  eventName = normalizeEventName(eventName);
  $eventNameOutput.val(eventName);

  if(eventProperties.length > 0){
    const eventPropertiesList = eventProperties.split(/\r?\n/);
    let rawData = '';
    $eventPropertiesOutput.html('');
    eventPropertiesList.map((e, key) => {
      const property = normalizeEventProperty(e);
      if(!property.length){
        return ;
      }
      $eventPropertiesOutput.append('<input  class="form-control" value="' + property + '" onclick="this.select();">');
      rawData += property + '\n';
    });
    if(eventPropertiesList.length > 1){
      $eventPropertiesOutputRaw.html(
        '<p>Raw Data</p>' +
        '<textarea class="form-control" onclick="this.select();" style="height: 100px;">' + rawData + '</textarea>'
      );
    }
  } else {
    $eventPropertiesOutput.html('No properties');
  }



});

/*
$("#utmBuilder").on('submit', function(){
  const websiteUrl = $("#websiteUrl").val(),
    campaignSource = $("#campaignSource").val(),
    campaignMedium = $("#campaignMedium").val(),
    campaignName = $("#campaignName").val(),
    campaignContent = $("#campaignContent").val(),
    date = $("#date").val();
  generateLink({
    websiteUrl,
    campaignSource,
    campaignMedium,
    campaignName,
    campaignContent,
    date
  });
});
*/

/*

$("#utmBuilder").parsley({
  errorClass: 'is-invalid text-danger',
  successClass: 'is-valid', // Comment this option if you don't want the field to become green when valid. Recommended in Google material design to prevent too many hints for user experience. Only report when a field is wrong.
  errorsWrapper: '<span class="form-text text-danger"></span>',
  errorTemplate: '<span></span>',
  trigger: 'change'
}).on('form:success', () => {
  const websiteUrl = $("#websiteUrl").val(),
    campaignSource = $("#campaignSource").val(),
    campaignMedium = $("#campaignMedium").val(),
    campaignName = $("#campaignName").val(),
    campaignContent = $("#campaignContent").val(),
    date = $("#date").val();
  const outputLink = generateLink({
    websiteUrl,
    campaignSource,
    campaignMedium,
    campaignName,
    campaignContent,
    date
  });

  const outputCampaignName = getCampaigName(campaignName, date);

  $("#outputLink").val(outputLink);
  $("#outputCampaignName").val(outputCampaignName);

  return false;

});

  */